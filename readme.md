Acabei começando o projeto de forma errada e precisei 
refazer quase tudo de novo e levei muito mais tempo que era pra ter levado, mas tá aí kkk.

Aquela ideia de "design mobile first" é muito boa quando a gente vai fazer do zero e com Grid. Mas quando vai fazer usando flexbox, que a gente precisa saber de antemão as estruturas aninhadas, na hora de fazer as queries acaba ficando bem mais complicado. **Principalmente** quando eu percebi que era apenas um design de celular (e não dois), e eu estava fazendo tudo olhando a imagem com zoom errado. E pra piorar, quando eu decidi refazer tudo de novo começando pela versão PC, eu por um bom tempo não tinha percebido que meu navegador estava com zoom e o padrão estava tudo minúsculo. 🤦‍♂️

Mas foi legal =)